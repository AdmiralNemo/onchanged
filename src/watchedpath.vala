namespace onchanged {

    public class WatchedPath : Object {
        public string path { get; construct; }
        public string[] events { get; construct; }
        public bool recursive { get; construct; }
        private string _action;
        private HashTable<string, FileMonitor> _monitors;

        construct {
            _monitors =
                new HashTable<string, FileMonitor>(str_hash, str_equal);

            var file = File.new_for_path(this.path);
            watch(file);
            var ftype = file.query_file_type(FileQueryInfoFlags.NONE);
            if (ftype == FileType.DIRECTORY && recursive) {
                watch_children(file);
            }
        }

        public WatchedPath(string path, string[] events,
                           bool recursive = true) {
            Object(
                path: path,
                events: events,
                recursive: recursive
            );
        }

        private void unwatch(File file) {
            var path = file.get_path();
            if (_monitors.get(path) != null) {
                if (onchanged.debug) {
                    stdout.printf("No longer watching path %s\n", path);
                }
                _monitors.remove(path);
            }
        }

        private void watch(File file) {
            var path = file.get_path();
            if (_monitors.get(path) == null) {
                if (onchanged.debug) {
                    stdout.printf("Watching path %s\n", path);
                    stdout.flush();
                }
                FileMonitor mon;
                try {
                    mon = file.monitor(FileMonitorFlags.NONE);
                } catch (GLib.Error e) {
                    stderr.printf(
                        "Failed to monitor %s: %s\n", path, e.message
                    );
                    stderr.flush();
                    return;
                }
                mon.changed.connect(handle_event);
                _monitors.insert(file.get_path(), mon);
            }
        }

        private void watch_children(File dir) {
            try {
                var enumerator = dir.enumerate_children(
                    FileAttribute.STANDARD_NAME,
                    FileQueryInfoFlags.NONE
                );

                FileInfo info;
                while ((info = enumerator.next_file()) != null) {
                    var f = dir.resolve_relative_path(info.get_name());
                    var ftype = f.query_file_type(FileQueryInfoFlags.NONE);
                    if (ftype == FileType.DIRECTORY) {
                        watch(f);
                        watch_children(f);
                    }
                }
            } catch (GLib.Error e) {
                stderr.printf("%s\n", e.message);
            }
        }

        private void handle_event(File file, File? other_file,
                                  FileMonitorEvent event_type) {
            if (onchanged.debug) {
                stdout.printf("%s: ", event_type.to_string());
                if (other_file != null) {
                    stdout.printf(
                        "%s, %s\n", file.get_path(), other_file.get_path()
                    );
                } else {
                    stdout.printf("%s\n", file.get_path());
                }
            }
            var file_type = file.query_file_type(FileQueryInfoFlags.NONE);
            switch (event_type) {
            case FileMonitorEvent.CREATED:
                if (file_type == FileType.DIRECTORY && recursive) {
                    watch(file);
                    watch_children(file);
                }
                do_action(file, "created");
                break;
            case FileMonitorEvent.CHANGES_DONE_HINT:
                do_action(file, "changed");
                break;
            case FileMonitorEvent.DELETED:
                unwatch(file);
                do_action(file, "deleted");
                break;
            }
        }

        private void do_action(File file, string event) {
            if (!(event in events)) {
                return;
            }

            string escaped_path = "'%s'".printf(
                file.get_path().replace("'", "'\\''")
            );
            string command = _action
                .replace("$#", escaped_path)
                .replace("$%", event);
            if (onchanged.debug) {
                stdout.printf("%s\n", command);
                stdout.flush();
            }
            try {
                string[] argv;
                Shell.parse_argv(command, out argv);
                Process.spawn_async("/", argv, null, SpawnFlags.SEARCH_PATH,
                                    null, null);
            } catch (GLib.ShellError e) {
                stderr.printf("Error parsing action: %s\n", e.message);
            } catch (GLib.SpawnError e) {
                stderr.printf("Error running action: %s\n", e.message);
            }
        }

        public void set_action(string action) {
            _action = action;
        }

    }

}
