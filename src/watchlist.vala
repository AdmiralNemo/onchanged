namespace onchanged {

    public class WatchList : Object {
        private List<WatchedPath> watches;

        construct {
            watches = new List<WatchedPath>();
        }

        public void read_config(string file) {
            if (onchanged.debug) {
                stdout.printf("Reading actions from %s\n", file);
                stdout.flush();
            }
            var conf = new KeyFile();
            conf.set_list_separator(',');
            try {
                conf.load_from_file(file, KeyFileFlags.NONE);
            } catch (FileError e) {
                stderr.printf("Failed to read %s: %s\n", file, e.message);
            } catch (KeyFileError e) {
                stderr.printf("Failed to parse %s: %s\n", file, e.message);
                return;
            }

            foreach (string path in conf.get_groups()) {
                string action;
                try {
                    action = conf.get_string(path, "action");
                } catch (KeyFileError e) {
                    stderr.printf(
                        "Error processing section %s: %s\n", path, e.message
                    );
                    continue;
                }

                string[]? events;
                try {
                    events = conf.get_string_list(path, "events");
                } catch (KeyFileError e) {
                    if (!(e is KeyFileError.KEY_NOT_FOUND)) {
                        stderr.printf(
                            "Warning processing section %s: %s\n", path,
                            e.message
                        );
                    }
                    events = {};
                }

                bool recursive;
                try {
                    recursive = conf.get_boolean(path, "recursive");
                } catch (KeyFileError e) {
                    if (!(e is KeyFileError.KEY_NOT_FOUND)) {
                        stderr.printf(
                            "Warning processing section %s: %s\n", path,
                            e.message
                        );
                    }
                    recursive = false;
                }

                var watch = new WatchedPath(path, events, recursive);
                watch.set_action(action);
                watches.append(watch);
            }
        }

        public void read_config_dir(File dir) {
            if (onchanged.debug) {
                stdout.printf(
                    "Reading all action files in %s\n", dir.get_path()
                );
            }

            try {
                var enumerator = dir.enumerate_children(
                    FileAttribute.STANDARD_NAME,
                    FileQueryInfoFlags.NONE
                );

                FileInfo info;
                while ((info = enumerator.next_file()) != null) {
                    var f = dir.resolve_relative_path(info.get_name());
                    var ftype = f.query_file_type(FileQueryInfoFlags.NONE);
                    if (ftype == FileType.DIRECTORY) {
                        read_config_dir(f);
                    } else {
                        read_config(f.get_path());
                    }
                }
            } catch (GLib.Error e) {
                stderr.printf("%s\n", e.message);
            }
        }

    }

}
