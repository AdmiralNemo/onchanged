namespace onchanged {
    public bool debug = false;

    public class Main : Object {
        [CCode(array_length = false, array_null_terminated = true)]
        private static string[]? action_files;

        private const OptionEntry[] options = {
            {"debug", 'D', 0, OptionArg.NONE, ref debug,
                "Write debug information to stdout", null},
            {"", 0, 0, OptionArg.FILENAME_ARRAY, ref action_files,
                "Action definition files", "[FILENAME [...]]"},
            { null },
        };

        private static void parse_args(string[] args) throws OptionError {
            action_files = {
                Path.build_filename(
                    Environment.get_user_config_dir(),
                    "onchanged"
                )
            };
            var ctx = new OptionContext();
            ctx.set_summary("Perform actions when files change");
            ctx.set_help_enabled(true);
            ctx.add_main_entries(options, null);
            ctx.parse(ref args);
        }

        public static int main(string[] args) {
            try {
                parse_args(args);
            } catch (OptionError e) {
                stderr.printf("error: %s\n", e.message);
                return 2;
            }

            var watches = new WatchList();
            foreach (string filename in action_files) {
                var f = File.new_for_commandline_arg(filename);
                switch(f.query_file_type(FileQueryInfoFlags.NONE)) {
                case FileType.UNKNOWN:
                    stderr.printf("Skipping missing file %s\n", f.get_path());
                    continue;
                case FileType.DIRECTORY:
                    watches.read_config_dir(f);
                    break;
                default:
                    watches.read_config(f.get_path());
                    break;
                }
            }

            var loop = new MainLoop();
            loop.run();

            return 0;
        }

    }

}
