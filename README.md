# onchanged

*onchanged* is a simple file watcher daemon that executes commands for
configured events on the filesystem. It is similar to [incron], but aims to be
simpler to use and provides additional features.


## Configuration

Configuration of *onchanged* is done through INI-style files that specify paths
to watch and commands to run. Example:

    [/home/dustin/Sync]
    recursive = true
    events = created,changed,deleted
    action = sync-offlinefiles

This configuration block instructs *onchanged* to watch the path
`/home/dustin/Sync` and run the command `sync-offlinefiles` when any event
occurs.

By default, configuration files are read from `${XDG_CONFIG_HOME}/onchanged`,
but alternate paths can be specified on the command line. If a directory is
specified, all files within the directory will be read.

The name of each section in a configuration file specifies the path to watch.
Valid options within a section are as follows:

* **recursive**: If `true` files in directories below the specified path will
  be watched as well (default `false`)
* **events**: A comma-separated list of events that will trigger the action. If
  unspecified, the action will never be triggered. Valid options are:
    - **created**: A new file was created in the directory
    - **changed**: An existing file's content or metadata was changed
    - **deleted**: A file was deleted from the directory
* **action**: The command to run when a matching event occurs. The executable
  must be found using the `${PATH}` environment variable. Variable expansion,
  I/O redirection, etc. will not be performed. Prior to execution, two
  placeholders will be substituted:
    - `$#`: The path to the file that triggered the action
    - `$%`: The event that occurred


## Building

To build *onchanged* from a source distribution, unpack the archive, then use
the standard three-step compile/install process:

    tar -xJf ./onchanged-0.1.tar.xz
    cd onchanged-0.1
    ./configure
    make
    sudo make install

You will need to the development files for [GLib] installed (*dev-libs/glib* on
Gentoo, *libglib2-dev* on Debian, or *glib2-devel* on Fedora).

To build from a Git checkout, you will need some additional packages:

* [Vala] version 0.20 or later (*dev-lang/vala* on Gentoo, *valac* on Debian,
  *vala* on Fedora)
* autoconf
* automake

The process for compiling/installing is similar:

    autoreconf -v --install .
    ./configure
    make
    sudo make install


## Contributing

Development of *onchanged* is ongoing. Please report bugs and suggestions to
the [issue tracker]. Patches and pull requests are welcome as well.

*onchanged* is licensed under the [GNU GPL version 3].


[incron]: http://inotify.aiken.cz/?section=incron&page=about&lang=en
[GLib]: https://developer.gnome.org/glib/
[Vala]: https://wiki.gnome.org/Projects/Vala
[issue tracker]: https://bitbucket.org/AdmiralNemo/onchanged/issues
[GNU GPL version 3]: https://www.gnu.org/licenses/gpl-3.0.html
